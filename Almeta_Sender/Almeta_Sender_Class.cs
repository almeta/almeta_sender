﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
//для формир и отправки файла
using System.IO;
using System.IO.Compression;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Timers;

namespace Almeta_Sender
{    
    public sealed class Almeta_Sender_Class
    {
        private static readonly Almeta_Sender_Class instance = new Almeta_Sender_Class();

        private Almeta_Sender_Class() { }

        public static Almeta_Sender_Class Instance
        {
            get
            {
                return instance;
            }
        }

        public  int GeneratePeriod;
        public  int DataPeriod;
        public  string UploadTime;
        public string LoadTypeDatas;
        public bool SendMail;
        public bool UploadingData;
        public string SubjectName;

        public string SendTimeCSD;
        public bool SendMailCSD;
        public string SubjectNameCSD;


        public string SMTPSettings;
        public string SMTPSenderServer;
        public string SMTPSenderPort;
        public string SMTPSenderAddress;
        public string SMTPSenderPassword;

        public string SMTPRecipientServer;
        public string SMTPRecipientPort;
        public string SMTPRecipientAddress;

        public string SMTPAuthorizationServer;
        public string FN_UploadSettings;
        public string SMTPLogin;
        public string SSL;

        public bool sendFileCSD(string fname)
       
        {

            if (ReadSettigsFromXML_UploadSettings() == true)
            {
                string subject = "";

               // subject = SubjectNameCSD;
                string MessageTime;

                MessageTime = DateTime.Now.ToString("dd-MM-yyyyTHH:mm:ss");

                //string body = @"Дата - " + MessageTime;

                string body = "";
                if (File.Exists(fname))
                {
                    body = File.ReadAllText(fname);
                }

                //subject = "L_20170722180000.xml";

                int i = 0;
                int lastSlashNum = 1;
                string s;
                while (i < fname.Length - 1)
                {
                    s = fname.Substring(i, 1);
                    if (s == "\\")
                    {
                        lastSlashNum = i;
                    }
                    i++;
                }
                subject = fname.Substring(lastSlashNum + 1, fname.Length - lastSlashNum - 1);

                MailMessage message = new MailMessage(SMTPSenderAddress, SMTPRecipientAddress, subject, body);


                try
                {
                    if (SMTPAuthorizationServer == "true")
                    {
                        SmtpClient mailClient = new SmtpClient(SMTPSenderServer, Convert.ToInt32(SMTPSenderPort));
                        if (SSL == "true")
                        {
                            mailClient.EnableSsl = true;
                        }
                        else
                            mailClient.EnableSsl = false;

                        mailClient.Credentials = new System.Net.NetworkCredential(SMTPLogin, SMTPSenderPassword);
                        mailClient.Send(message);
                    }
                    else
                    {
                        SmtpClient client = new SmtpClient(SMTPSenderServer, Convert.ToInt32(SMTPSenderPort));
                        client.Credentials = CredentialCache.DefaultNetworkCredentials;
                        client.Send(message);

                    }

                    return (true);
                }
                catch (Exception e)
                {
                    string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "sendFileCSD_log.txt";
                    StreamWriter SendDatasSCD_log = new StreamWriter(logpath, true);
                    SendDatasSCD_log.WriteLine("error send message: " + MessageTime + " " + subject);
                    SendDatasSCD_log.WriteLine(e.ToString());
                    SendDatasSCD_log.Close();
                    return (false);
                }
            }
            else
            {
                string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "sendFileCSD_log.txt";
                StreamWriter SendDatasSCD_log = new StreamWriter(logpath, true);
                SendDatasSCD_log.WriteLine("error : " + DateTime.Now.ToString() + " ошибка чтения настроек для отправки");
                SendDatasSCD_log.Close();
                return (false);
            }

        }

        public bool ReadSettigsFromXML_UploadSettings()
        {
            bool res = true;
            //FN_UploadSettings = System.AppDomain.CurrentDomain.BaseDirectory;

            //int i = 0;
            //int lastSlashNum = 1;
            //string s;
            //while (i < FN_UploadSettings.Length - 1)
            //{
            //    s = FN_UploadSettings.Substring(i, 1);
            //    if (s == "\\")
            //    {
            //        lastSlashNum = i;
            //    }
            //    i++;
            //}
            //s = FN_UploadSettings.Substring(0, lastSlashNum + 1);
            //FN_UploadSettings = s + "SendSettings.xml"; 
            FN_UploadSettings = "C:\\Program Files (x86)\\LANIT\\Almeta\\SendSettings.xml";


            string XMLUploadSettings = "UploadSettings.xml";

            try
            {
                if (File.Exists(FN_UploadSettings) == true)
                {
                    StreamReader XMLStream = new StreamReader(FN_UploadSettings);

                    XMLUploadSettings = XMLStream.ReadToEnd();


                    XMLStream.Close();

                    XElement xmlTree = XElement.Parse(XMLUploadSettings);


                    XName XUploadData = "UploadData";
                    XName XGeneratePeriod = "GeneratePeriod";
                    XName XDataPeriod = "DataPeriod";
                    XName XUploadingData = "UploadingData";
                    XName XUploadTime = "UploadTime";
                    XName XSendMail = "SendMail";
                    XName XSubjectName = "SubjectName";
                    XName XLoadTypeDatas = "LoadTypeDatas";

                    XName XSendCSDSettings = "SendCSDSettings";
                    XName XSendTime = "SendTime";
                    XName XSendMailCSD = "SendMailCSD";
                    XName XSubjectNameCSD = "SubjectNameCSD";

                    XName XSMTPSettings = "SMTPSettings";
                    XName XSMTPSender = "SMTPSender";
                    XName XSMTPRecipient = "SMTPRecipient";
                    XName XSMTPServer = "SMTPServer";
                    XName XSMTPPort = "SMTPPort";
                    XName XSMTPAddress = "SMTPAddress";
                    XName XSMTPPassword = "SMTPPassword";
                    XName XLogin = "Login";

                    XName XSSL = "SSL";

                    XName XSubjectText = "SubjectText";
                    XName XBodyText = "BodyText";

                    XName XAuthorizationServer = "AuthorizationServer";

                    SSL = "false";
                    try
                    {

                        UploadingData = System.Convert.ToBoolean(xmlTree.Element(XUploadData).Element(XUploadingData).Value);
                        SubjectName = xmlTree.Element(XUploadData).Element(XSubjectName).Value;

                        GeneratePeriod = System.Convert.ToInt16(xmlTree.Element(XUploadData).Element(XGeneratePeriod).Value);
                        DataPeriod = System.Convert.ToInt16(xmlTree.Element(XUploadData).Element(XDataPeriod).Value);
                        UploadTime = xmlTree.Element(XUploadData).Element(XUploadTime).Value;
                        SendMail = System.Convert.ToBoolean(xmlTree.Element(XUploadData).Element(XSendMail).Value);
                        LoadTypeDatas = xmlTree.Element(XUploadData).Element(XLoadTypeDatas).Value;

                        SendTimeCSD = xmlTree.Element(XSendCSDSettings).Element(XSendTime).Value;
                        SendMailCSD = System.Convert.ToBoolean(xmlTree.Element(XSendCSDSettings).Element(XSendMailCSD).Value);
                        SubjectNameCSD = xmlTree.Element(XSendCSDSettings).Element(XSubjectNameCSD).Value;

                        SMTPSenderServer = xmlTree.Element(XSMTPSettings).Element(XSMTPSender).Element(XSMTPServer).Value;
                        SMTPSenderPort = xmlTree.Element(XSMTPSettings).Element(XSMTPSender).Element(XSMTPPort).Value;
                        SMTPSenderAddress = xmlTree.Element(XSMTPSettings).Element(XSMTPSender).Element(XSMTPAddress).Value;
                        SMTPSenderPassword = xmlTree.Element(XSMTPSettings).Element(XSMTPSender).Element(XSMTPPassword).Value;
                        try
                        {
                            SSL = xmlTree.Element(XSMTPSettings).Element(XSMTPSender).Element(XSSL).Value;
                        }
                        catch (Exception ex)
                        {
                            string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "sendFileCSD_log.txt";
                            StreamWriter SendDatasSCD_log = new StreamWriter(logpath, true);
                            SendDatasSCD_log.WriteLine("error : " + DateTime.Now.ToString() + " " + ex.Message);
                            SendDatasSCD_log.Close();
                        }
                        try
                        {
                            SMTPLogin = xmlTree.Element(XSMTPSettings).Element(XSMTPSender).Element(XLogin).Value;
                        }
                        catch (Exception ex)
                        {

                        }

                        SMTPRecipientServer = xmlTree.Element(XSMTPSettings).Element(XSMTPRecipient).Element(XSMTPServer).Value;
                        SMTPRecipientPort = xmlTree.Element(XSMTPSettings).Element(XSMTPRecipient).Element(XSMTPPort).Value;
                        SMTPRecipientAddress = xmlTree.Element(XSMTPSettings).Element(XSMTPRecipient).Element(XSMTPAddress).Value;

                        SMTPAuthorizationServer = xmlTree.Element(XSMTPSettings).Attribute(XAuthorizationServer).Value;
                    }
                    catch (Exception ex)
                    {
                        string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "sendFileCSD_log.txt";
                        StreamWriter SendDatasSCD_log = new StreamWriter(logpath, true);
                        SendDatasSCD_log.WriteLine("error : " + DateTime.Now.ToString() + " " + ex.Message);
                        SendDatasSCD_log.Close();
                    }
                }
                else
                {
                    res = false;

                }
            }
            catch (Exception ex)
            {
                string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "sendFileCSD_log.txt";
                StreamWriter SendDatasSCD_log = new StreamWriter(logpath, true);
                SendDatasSCD_log.WriteLine("error : " + DateTime.Now.ToString() + " " + ex.Message);
                SendDatasSCD_log.Close();
            }

            return res;
        }


    }
}
